#include <math.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <TimerOne.h>
#include <SPI.h>
#include <max6675.h>

#define START 2
#define SUM 3
#define RES 4
#define MODE 5
#define TEMPO 6
#define MAXDO   7
#define MAXCS   8
#define MAXCLK  9
#define PULSOS 10

//cambiar nombre y librería al max6675 para probarlo en fisico
//max31855 solo para uso en simulación
MAX6675 thermocouple(MAXCLK, MAXCS, MAXDO);

LiquidCrystal_I2C lcd(0x27,16,2);

volatile long int tiempo = 0;
byte temp = 0;
byte tempdeseado = 0;
byte numMenu = 0;
byte numStart = 0;
byte pre = 0;

bool flag = true;

byte estadoPulsos = LOW;
unsigned long tiempoAnt = 0;
const long periodo = 1000; 

byte BSTART = 0;
byte BSUM = 0;
byte BRES = 0;
byte BMODE = 0;
byte BTEMPO = 0;

byte selector = 0;
byte conteostart = 0;
boolean isPressed = false; 

void setup() {
  lcd.init();
  lcd.backlight();

  
  lcd.clear();
  lcd.setCursor(3,0);
  lcd.print("BIENVENIDO");
  lcd.setCursor(2,1);
  lcd.print("A  KOLORTIPS");
  delay(2000);
  lcd.clear();
  lcd.print("Inicializando.");
  delay(500);
  lcd.clear();
  lcd.print("Inicializando..");
  delay(500);
  lcd.clear();
  lcd.print("Inicializando...");
  delay(500);
  lcd.clear();

  pinMode(START, INPUT_PULLUP);
  pinMode(SUM, INPUT_PULLUP);
  pinMode(RES, INPUT_PULLUP);
  pinMode(MODE, INPUT_PULLUP);
  pinMode(TEMPO, INPUT_PULLUP);
  pinMode(PULSOS, OUTPUT);

}

void loop() {
  switchMenu();
  
  temp = thermocouple.readCelsius();
  
  BSTART = digitalRead(START);
  BSUM = digitalRead(SUM);
  BRES = digitalRead(RES);
  BMODE = digitalRead(MODE);
  BTEMPO = digitalRead(TEMPO);

  if(BTEMPO == LOW){
    Timer1.initialize(1000000);
    Timer1.attachInterrupt(temporizador);
  }


  if(BMODE == LOW && isPressed == false){
    isPressed = true;
    
    selector++;

    if(selector > 3){
      selector = 0;
    }
    delay(200);
  }
  else if(BMODE == HIGH){
    isPressed = false;
    delay(200);
  }

  if(numMenu == 1){
    conteo1();
  }
  else if(numMenu == 2){
    conteo2();
  }
  else if(numMenu == 3){
    conteo3();
  }
  else if(numMenu == 4){
    conteo4();
  }




  if(BSTART == LOW && isPressed == false){
    isPressed = true;
    switchStart();
    conteostart++;

    if(conteostart > 2){
      conteostart = 0;
    }
    delay(200);
  }
  else if(BSTART == HIGH){
    isPressed = false;
    delay(200);
  }

  if(numStart == 1){
    conteostart1();
  }
  else if(numStart == 2){
    conteostart2();
  }
}



void switchStart() {
  switch(conteostart) {
    case 0:
    numStart = 1;
    break;
    case 1:
    numStart = 2;
    break;
  }
}


void conteostart1() {
    unsigned long tiempoAho = millis();
    
    if (tiempoAho - tiempoAnt >= periodo && flag == true) {
    tiempoAnt = tiempoAho;
    
    if(estadoPulsos == LOW && temp < pre) {
      estadoPulsos = HIGH;
    }
    else {
      estadoPulsos = LOW;
    }
    digitalWrite(PULSOS, estadoPulsos);

  if(temp >= pre){
    flag = false;
    tiempoAnt = millis();
  }
  }
  else {
    flag = true;
  }


  }


void conteostart2() {
    unsigned long tiempoAho = millis();
    
    if (tiempoAho - tiempoAnt >= periodo && flag == true) {
    tiempoAnt = tiempoAho;

    if(estadoPulsos == LOW && temp < tempdeseado) {
      estadoPulsos = HIGH;
    }
    else {
      estadoPulsos = LOW;
    }
    digitalWrite(PULSOS, estadoPulsos);
    
  if(temp >= tempdeseado){
    flag = false;                 
    tiempoAnt = millis();
  }
  }
    else {
    flag = true;
  }

}


void switchMenu() {
  switch(selector) {
  case 0:
    numMenu = 1;
    break;
  case 1:
    numMenu = 2;
    break;
  case 2:
    numMenu = 3;
    break;
  case 3:
    numMenu = 4;
    break;
  }
}

void conteo1() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ingrese Temper:");
  lcd.setCursor(0,1);
  lcd.print((char)223);
  lcd.print("C:");
  lcd.print(tempdeseado);
  lcd.print(" ");
  
    if(BSUM == LOW && tiempo >= 0){
      tempdeseado++;
      delay(200);
    }
    else if(BRES == LOW && tiempo > 0){
      tempdeseado--;
      delay(200);
    }
}

void conteo2() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ingrese Tiempo:");
  lcd.setCursor(0,1);
  lcd.print(tiempo);
  lcd.print(" Segundos");
  
    if(BSUM == LOW && tiempo >= 0){
      tiempo++;
      delay(200);
    }
    else if(BRES == LOW && tiempo > 0){
      tiempo--;
      delay(200);
    }
}

void conteo3() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ingrese Preca:");
  lcd.setCursor(0,1);
  lcd.print((char)223);
  lcd.print("C:");
  lcd.print(pre);
  lcd.print(" ");
  
    if(BSUM == LOW && tiempo >= 0){
      pre++;
      delay(200);
    }
    else if(BRES == LOW && tiempo > 0){
      pre--;
      delay(200);
    }
}

void conteo4() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Temper: ");
  lcd.setCursor(9,0);
  lcd.print(temp);
  lcd.print(" ");
  lcd.print((char)223);
  lcd.print("C");
  lcd.setCursor(0,1);
  lcd.print("Tiempo: ");
  lcd.print(tiempo);
  lcd.print(" Seg");
}

void temporizador(void) {
  if(tiempo > 0){
    tiempo--;
  }
  else{
    tiempo = 0;
    Timer1.stop();
  }
}
